/**
 * Ojini Barsoumian 
 * 1840580
 * BikeStore Java Class.
 */
package application;

import vehicles.Bicycle;

public class BikeStore {

    public static void main(String[] args) {
        final int SIZE_ARRAY = 4;

        Bicycle[] bikes = new Bicycle[SIZE_ARRAY];
        bikes[0] = new Bicycle("ARG", 50, 400);
        bikes[1] = new Bicycle("BNA", 56, 300);
        bikes[2] = new Bicycle("HJL", 70, 400);
        bikes[3] = new Bicycle("OLY", 80, 900);

        for (int i = 0; i < bikes.length; i++) {
            System.out.println(bikes[i] + ". ");
        }

    }
}
