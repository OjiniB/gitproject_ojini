/**
 * Ojini Barsoumian 
 * 1840580
 * Bicycle Java Class.
 */
package vehicles;

public class Bicycle {
    private String manufacturer;
    private int numberOfGears;
    private double maxSpeed;

    public Bicycle(String manufactur, int numbers, double speed) {
        this.manufacturer = manufactur;
        this.numberOfGears = numbers;
        this.maxSpeed = speed;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public int getNumberOfGears() {
        return this.numberOfGears;
    }

    public double getMaxSpeed() {
        return this.maxSpeed;
    }

    public String toString() {
        return "Manufacturer: " + this.manufacturer + ", number Of Gears: " + this.numberOfGears + ", Max Speed: "
                + this.maxSpeed;
    }

}
